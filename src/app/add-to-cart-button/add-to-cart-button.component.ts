import { Component, Input, OnInit } from '@angular/core';
import { faCartPlus, faHeart } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-add-to-cart-button',
  templateUrl: './add-to-cart-button.component.html',
  styleUrls: ['./add-to-cart-button.component.scss']
})
export class AddToCartButtonComponent implements OnInit {

  faCartPlus = faCartPlus;
  faHeart = faHeart;
  constructor() { }

  /**
* Is this the principal call to action on the page?
*/
  @Input()
  buttonBackgroundColor: string = "#005CBF";

  /**
* Is this the principal call to action on the page?
*/
  @Input()
  addToCartColor: string = "#005CBF";


  /**
* Is this the principal call to action on the page?
*/
  @Input()
  wishListColor: string = "#005CBF";

  ngOnInit(): void {
  }

}
