import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { FeaturedProductComponent } from './featured-product/featured-product.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { HomePageComponent } from './home-page/home-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LogoComponent } from './logo/logo.component';
import { SearchComponent } from './search/search.component';
import { ShopNowButtonComponent } from './shop-now-button/shop-now-button.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AddToCartButtonComponent } from './add-to-cart-button/add-to-cart-button.component';
import { NavbarIconsComponent } from './navbar-icons/navbar-icons.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    FeaturedProductComponent,
    FooterComponent,
    HeaderComponent,
    NavbarComponent,
    ProductDetailComponent,
    HomePageComponent,
    PageNotFoundComponent,
    LogoComponent,
    SearchComponent,
    ShopNowButtonComponent,
    AddToCartButtonComponent,
    NavbarIconsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
