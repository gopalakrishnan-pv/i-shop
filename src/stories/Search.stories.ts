// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/angular/types-6-0';
import { SearchComponent } from '../app/search/search.component';


export default {
  title: 'Header/Search',
  component: SearchComponent,
  argTypes: {
    buttonBackgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<SearchComponent> = (args: SearchComponent) => ({
  component: SearchComponent,
  props: args,
});

export const Search = Template.bind({});
Search.args = {
  buttonBackgroundColor: '#28a746',
  // buttonBackgroundColor: 'black',
  searchPlaceholderText: "Search Products..."
};
