import { Component, Input, OnInit } from '@angular/core';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-shop-now-button',
  templateUrl: './shop-now-button.component.html',
  styleUrls: ['./shop-now-button.component.scss'],

})
export class ShopNowButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  faShoppingCart = faShoppingCart;

  /**
 * Background color of the button
 */
  @Input()
  buttonBackgroundColor: string = "#005CBF";

  /**
 * Font family of the button
 */
  @Input()
  buttonFont: string = "Arial";
  // buttonFont: string = "Monospace";

}
