import { Component, OnInit } from '@angular/core';
import { ProductModel } from '../../models/product';
import { faStar, faUser } from '@fortawesome/free-solid-svg-icons';
import { ProductService } from '../product.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  faStar = faStar;
  faUser = faUser;
  products: ProductModel[] = [];


  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit(): void {
    this.products = this.productService.products;
    localStorage.setItem('selectedProduct', '');
  }

  getArray(n: number): any[] {
    return Array(n);
  }
  getDifferenceArray(n: number, d: number): any[] {
    return Array(d - n);
  }

  navigateToProductDetail(product: ProductModel): void {
    this.productService.selectedProduct = product;
    this.router.navigate(['/product-detail']);
  }
}
