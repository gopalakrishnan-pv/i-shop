export class ProductModel {
  name: string
  description: string
  detailedDescription: string
  price: number
  thumbnailImage: string
  discount: number
  rating: number
  reviewersCount: number

  constructor(name: string, description: string, detailedDescription: string, price: number, thumbnailImage: string, discount: number, rating: number, reviewersCount: number) {
    this.name = name
    this.description = description
    this.detailedDescription = detailedDescription
    this.price = price
    this.thumbnailImage = thumbnailImage;
    this.discount = discount;
    this.rating = rating;
    this.reviewersCount = reviewersCount;
  }
}