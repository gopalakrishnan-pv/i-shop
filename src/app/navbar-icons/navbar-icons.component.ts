import { Component, Input, OnInit } from '@angular/core';
import { faShoppingCart, faHeart, faUser } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navbar-icons',
  templateUrl: './navbar-icons.component.html',
  styleUrls: ['./navbar-icons.component.scss']
})
export class NavbarIconsComponent implements OnInit {

  faShoppingCart = faShoppingCart;
  faHeart = faHeart;
  faUser = faUser;


  /**
* Color of the shopping cart icon
*/
  @Input()
  shoppingCartIconColor: string = "#FFFFFF";

  /**
  * Color of the wishlist icon
  */
  @Input()
  wishListIconColor: string = "#FFFFFF";

  /**
  * Color of the user icon
  */
  @Input()
  userIconColor: string = "#FFFFFF";

  constructor() { }

  ngOnInit(): void {
  }

}
