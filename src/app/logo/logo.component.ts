import { Component, Input, OnInit } from '@angular/core';
import { faShoppingCart, faHeart, faUser } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {

  faShoppingCart = faShoppingCart;

  constructor() { }

  ngOnInit(): void {
  }

  /**
 * Logo color of the application
 */
  @Input()
  logoIconColor: string = "#FBDE4E";


  /**
* Logo text color of the application
*/
  @Input()
  logoTextColor: string = "#FBDE4E";

  /**
* Logo text color of the application
*/
  @Input()
  logoFontSize: number = 20;
  // logoFontSize: number = 25;

}
