import { Component, OnInit } from '@angular/core';
import { faCartPlus, faHeart, faStar, faUser } from '@fortawesome/free-solid-svg-icons';
import { ProductModel } from '../../models/product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  faCartPlus = faCartPlus;
  faHeart = faHeart;
  faStar = faStar;
  faUser = faUser;
  product: ProductModel = <ProductModel>{};
  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    if (localStorage.getItem('selectedProduct') == '') {
      this.product = this.productService.selectedProduct;
      localStorage.setItem('selectedProduct', JSON.stringify(this.product));
    } else {
      // this.product = JSON.parse(localStorage.getItem('selectedProduct'));
    }
  }


  getArray(n: number): any[] {
    return Array(n);
  }
  getDifferenceArray(n: number, d: number): any[] {
    return Array(d - n);
  }


}
