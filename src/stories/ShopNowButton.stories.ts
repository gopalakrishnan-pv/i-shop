// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/angular/types-6-0';
import { ShopNowButtonComponent } from '../app/shop-now-button/shop-now-button.component'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { APP_INITIALIZER } from '@angular/core';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { moduleMetadata } from '@storybook/angular';

export default {
  title: 'Buttons/ShopNowButton',
  component: ShopNowButtonComponent,
  argTypes: {
    buttonBackgroundColor: { control: 'color' },
    buttonFont: {
      control: {
        type: 'select',
        options: [
          'Lucida Console', 'Courier New', 'monospace', 'Arial', 'Times New Roman'
        ],
      },
    }
  },
  decorators: [
    moduleMetadata({
      imports: [FontAwesomeModule],
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: (iconLibrary: FaIconLibrary) => async () => {
            // Add any icons needed here:
            iconLibrary.addIcons(faShoppingCart);
          },
          // When using a factory provider you need to explicitly specify its dependencies.
          deps: [FaIconLibrary],
          multi: true,
        },
      ],
    }),
  ],
};
const Template: Story<ShopNowButtonComponent> = (args: ShopNowButtonComponent) => ({
  component: ShopNowButtonComponent,
  props: args,
});

export const ShopNowButton = Template.bind({});
ShopNowButton.args = {
  buttonBackgroundColor: '#005CBF',
  buttonFont: 'Arial'
  // buttonFont: 'Monospace'
};
