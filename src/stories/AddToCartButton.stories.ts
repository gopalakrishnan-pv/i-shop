// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/angular/types-6-0';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { APP_INITIALIZER } from '@angular/core';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { moduleMetadata } from '@storybook/angular';
import { AddToCartButtonComponent } from '../app/add-to-cart-button/add-to-cart-button.component';

export default {
  title: 'Buttons/AddToCartButton',
  component: AddToCartButtonComponent,
  argTypes: {
    addToCartColor: { control: 'color' },
    wishListColor: { control: 'color' },
  },
  decorators: [
    moduleMetadata({
      imports: [FontAwesomeModule],
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: (iconLibrary: FaIconLibrary) => async () => {
            // Add any icons needed here:
            iconLibrary.addIcons(faShoppingCart);
          },
          // When using a factory provider you need to explicitly specify its dependencies.
          deps: [FaIconLibrary],
          multi: true,
        },
      ],
    }),
  ],
};
const Template: Story<AddToCartButtonComponent> = (args: AddToCartButtonComponent) => ({
  component: AddToCartButtonComponent,
  props: args,
});

export const AddToCartButton = Template.bind({});
AddToCartButton.args = {
  addToCartColor: '#005CBF',
  wishListColor: '#005CBF'
};
