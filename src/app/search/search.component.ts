import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  /**
 * Background color of the button
 */
  @Input()
  buttonBackgroundColor: string = "#28a745";
  // buttonBackgroundColor: string = "black";

  /**
* Placeholder text of search box
*/
  @Input()
  searchPlaceholderText: string = "Search Products...";
}
