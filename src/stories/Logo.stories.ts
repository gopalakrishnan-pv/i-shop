// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story } from '@storybook/angular/types-6-0';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { APP_INITIALIZER } from '@angular/core';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { moduleMetadata } from '@storybook/angular';
import { LogoComponent } from '../app/logo/logo.component';

export default {
  title: 'Header/Logo',
  component: LogoComponent,
  argTypes: {
    logoIconColor: { control: 'color' },
    logoTextColor: { control: 'color' },
    logoFontSize: { control: { type: 'range', min: 10, max: 100, step: 10 } }
  },
  decorators: [
    moduleMetadata({
      imports: [FontAwesomeModule],
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: (iconLibrary: FaIconLibrary) => async () => {
            // Add any icons needed here:
            iconLibrary.addIcons(faShoppingCart);
          },
          // When using a factory provider you need to explicitly specify its dependencies.
          deps: [FaIconLibrary],
          multi: true,
        },
      ],
    }),
  ],
};

const Template: Story<LogoComponent> = (args: LogoComponent) => ({
  component: LogoComponent,
  props: args,
});

export const Logo = Template.bind({});
Logo.args = {
  logoIconColor: '#FBDE4E',
  logoTextColor: '#FBDE4E',
  logoFontSize: 20
  // logoFontSize: 25
};
