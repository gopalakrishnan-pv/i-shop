import { Injectable } from '@angular/core';
import { ProductModel } from '../models/product';
import data from '../assets/data/products.json';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products: ProductModel[] = data;
  selectedProduct: ProductModel = <ProductModel>{};


  constructor() { }
}
